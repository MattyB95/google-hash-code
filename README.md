# Google Hash Code

## Google Hash Code 2018

### Ranks

**University of Kent - #1**

**United Kingdom - #71**

**Competition - #897**

![Google Hash Code 2018](./2018/Qualification%20Round/documents/2018Certificate.jpg "Google Hash Code 2018")

## Google Hash Code 2021

### Ranks

**University of Kent - #1**

**United Kingdom - #193**

**Competition - #5348**

![Google Hash Code 2021](./2021/Qualification%20Round/documents/2021Certificate.jpg "Google Hash Code 2021")

### Ranks (Extended Round)

**University of Kent - #1**

**United Kingdom - #24**

**Competition - #757**
