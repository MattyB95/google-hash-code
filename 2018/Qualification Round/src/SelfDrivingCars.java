import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

public class SelfDrivingCars {

    private static final String SEPARATOR = " ";

    private final List<Car> cars = new ArrayList<>();
    private final List<Ride> rides = new ArrayList<>();

    public static void main(String[] args) throws IOException, URISyntaxException {
        new SelfDrivingCars("a_example.in");
        new SelfDrivingCars("b_should_be_easy.in");
        new SelfDrivingCars("c_no_hurry.in");
        new SelfDrivingCars("d_metropolis.in");
        new SelfDrivingCars("e_high_bonus.in");
    }

    private SelfDrivingCars(String fileName) throws URISyntaxException, IOException {
        URI uri = Objects.requireNonNull(getClass().getClassLoader().getResource(fileName)).toURI();
        File pizzaFile = new File(uri);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(pizzaFile));
        String line = bufferedReader.readLine();
        List<String> fileInformation = Arrays.asList(line.split(SEPARATOR));

        int numberOfRows = Integer.parseInt(fileInformation.get(0));
        int numberOfColumns = Integer.parseInt(fileInformation.get(1));
        int numberOfVehicles = Integer.parseInt(fileInformation.get(2));
        int numberOfRides = Integer.parseInt(fileInformation.get(3));
        int startBonus = Integer.parseInt(fileInformation.get(4));
        int simulationSteps = Integer.parseInt(fileInformation.get(5));

        getCars(numberOfVehicles);
        getRides(bufferedReader);

        firstTime();

        List<Integer> scheduledRides = new ArrayList<>();
        while (scheduledRides.size() < numberOfRides) {
            cars.forEach(car -> {
                if (scheduledRides.size() < numberOfRides) {
                    int shortestJourney;
                    do {
                        shortestJourney = car.findShortestJourney();
                        if (scheduledRides.contains(shortestJourney)) {
                            car.totalJourneyDistance.set(shortestJourney, Integer.MAX_VALUE);
                        }
                    } while (scheduledRides.contains(shortestJourney));

                    car.rideIndex.add(shortestJourney);
                    car.currentRow = rides.get(shortestJourney).finishRow;
                    car.currentColumn = rides.get(shortestJourney).finishColumn;
                    scheduledRides.add(shortestJourney);
                }
            });
            updateJourneyDistances();
        }

        List<String> lines = new ArrayList<>();
        cars.forEach(car -> {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(car.rideIndex.size()).append(SEPARATOR);
            car.rideIndex.forEach(rideIndex -> stringBuilder.append(rideIndex).append(SEPARATOR));
            lines.add(stringBuilder.toString());
        });
        System.out.println(lines);
        Path file = Paths.get("Qualification Round/out/" + fileName);
        Files.createDirectories(file.getParent());
        Files.write(file, lines, StandardOpenOption.CREATE);
    }

    private void firstTime() {
        cars.forEach(car -> {
            for (Ride ride : rides) {
                int changeInX = Math.abs(car.currentRow - ride.startRow);
                int changeInY = Math.abs(car.currentColumn - ride.startColumn);
                int distanceToStart = changeInX + changeInY;
                int totalDistance = distanceToStart + ride.rideDistance + ride.earliestStart;
                car.distanceToStart.add(distanceToStart);
                car.totalJourneyDistance.add(totalDistance);
            }
        });
    }

    private void updateJourneyDistances() {
        cars.forEach(car -> {
            int index = 0;
            for (Ride ride : rides) {
                int changeInX = Math.abs(car.currentRow - ride.startRow);
                int changeInY = Math.abs(car.currentColumn - ride.startColumn);
                int distanceToStart = changeInX + changeInY;
                int totalDistance = distanceToStart + ride.rideDistance + ride.earliestStart;
                car.distanceToStart.set(index, distanceToStart);
                if (!(car.totalJourneyDistance.get(index) == Integer.MAX_VALUE)) {
                    car.totalJourneyDistance.set(index, totalDistance);
                }
                index++;
            }
        });
    }

    private void getCars(int numberOfVehicles) {
        IntStream.range(0, numberOfVehicles).forEach(car -> cars.add(new Car()));
    }

    private void getRides(BufferedReader bufferedReader) {
        bufferedReader.lines().forEach(line1 -> {
            List<String> rideInfo = Arrays.asList(line1.split(SEPARATOR));
            int startRow = Integer.parseInt(rideInfo.get(0));
            int startColumn = Integer.parseInt(rideInfo.get(1));
            int finishRow = Integer.parseInt(rideInfo.get(2));
            int finishColumn = Integer.parseInt(rideInfo.get(3));
            int earliestStart = Integer.parseInt(rideInfo.get(4));
            int latestFinish = Integer.parseInt(rideInfo.get(5));
            Ride ride = new Ride(startRow, startColumn, finishRow, finishColumn, earliestStart, latestFinish);
            rides.add(ride);
        });
    }

}
