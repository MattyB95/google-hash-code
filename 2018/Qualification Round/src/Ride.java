public class Ride {

    public int startRow;
    public int startColumn;
    public int finishRow;
    public int finishColumn;
    public int earliestStart;
    public int latestFinish;
    public int rideDistance;

    public Ride(int startRow, int startColumn, int finishRow, int finishColumn, int earliestStart, int latestFinish) {
        this.startRow = startRow;
        this.startColumn = startColumn;
        this.finishRow = finishRow;
        this.finishColumn = finishColumn;
        this.earliestStart = earliestStart;
        this.latestFinish = latestFinish;
        calculateRideDistance();
    }

    private void calculateRideDistance() {
        int changeInX = Math.abs(finishRow - startRow);
        int changeInY = Math.abs(finishColumn - startColumn);
        rideDistance = changeInX + changeInY;
    }

}
