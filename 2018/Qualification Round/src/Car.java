import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Car {

    public int currentRow = 0;
    public int currentColumn = 0;

    public int currentStep = 0;

    public List<Integer> distanceToStart = new ArrayList<>();
    public List<Integer> totalJourneyDistance = new ArrayList<>();
    public List<Integer> rideIndex = new ArrayList<>();

    public int findShortestJourney() {
        return totalJourneyDistance.indexOf(Collections.min(totalJourneyDistance));
    }

}
