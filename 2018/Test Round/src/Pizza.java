import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Pizza {

    private static final String EXAMPLE = "example.in";
    private static final String SEPARATOR = " ";
    private static final char MUSHROOM = 'M';
    private static final char TOMATO = 'T';

    public static void main(String[] args) throws URISyntaxException, IOException {
        new Pizza();
    }

    Pizza() throws URISyntaxException, IOException {
        URI uri = Objects.requireNonNull(getClass().getClassLoader().getResource(EXAMPLE)).toURI();
        File pizzaFile = new File(uri);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(pizzaFile));
        String line = bufferedReader.readLine();
        List<String> fileInformation = Arrays.asList(line.split(SEPARATOR));

        int numberOfRows = Integer.parseInt(fileInformation.get(0));
        int numberOfColumns = Integer.parseInt(fileInformation.get(1));
        int minimumIngredientNumber = Integer.parseInt(fileInformation.get(2));
        int maximumCellNumber = Integer.parseInt(fileInformation.get(3));

        int[][] mask = new int[numberOfRows][numberOfColumns];
        char[][] pizza = new char[numberOfRows][numberOfColumns];

        for (int indexI = 0; indexI < numberOfRows; indexI++) {
            String pizzaLine = bufferedReader.readLine();
            char[] pizzaElements = pizzaLine.toCharArray();
            pizza[indexI] = pizzaElements;
        }

    }

    boolean doesSliceContainMinimumNumberOfIngredients(Character[][] pizzaSlice, int minimumIngredientNumber) {
        boolean correctAmountOfMushrooms = amountOfIngredient(pizzaSlice, MUSHROOM) >= minimumIngredientNumber;
        boolean correctAmountOfTomatoes = amountOfIngredient(pizzaSlice, TOMATO) >= minimumIngredientNumber;
        return correctAmountOfTomatoes && correctAmountOfMushrooms;
    }

    long amountOfIngredient(Character[][] pizzaSlice, Character ingredient) {
        return Arrays.stream(pizzaSlice)
                .flatMap(Arrays::stream)
                .filter(character -> character == ingredient)
                .count();
    }

}
