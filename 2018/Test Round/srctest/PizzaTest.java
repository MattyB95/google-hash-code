import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.*;

class PizzaTest {

    public static final char TOMATO = 'T';
    public static final char MUSHROOM = 'M';
    private Pizza pizza;

    @BeforeEach
    void setUp() throws IOException, URISyntaxException {
        pizza = new Pizza();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void whenPizzaSliceContainsCorrectIngredientsAmountThenDoesSliceContainMinimumNumberOfIngredientsReturnsTrue() {
        int minimumIngredientNumber = 1;
        Character[][] validSlice = new Character[][]{
                {'T', 'T', 'T'},
                {'T', 'M', 'T'},
        };
        assertTrue(pizza.doesSliceContainMinimumNumberOfIngredients(validSlice, minimumIngredientNumber));
    }

    @Test
    void whenPizzaSliceContainsIncorrectIngredientsAmountThenDoesSliceContainMinimumNumberOfIngredientsReturnsFalse() {
        int minimumIngredientNumber = 1;
        Character[][] validSlice = new Character[][]{
                {'T', 'T', 'T'},
                {'T', 'T', 'T'},
        };
        assertFalse(pizza.doesSliceContainMinimumNumberOfIngredients(validSlice, minimumIngredientNumber));
    }

    @Test
    void whenNumberOfTomatoesInSliceIsFourThenAmountOfIngredientReturnsFour() {
        Character[][] sliceWithFourTomatoes = new Character[][]{
                {'T', 'T', 'T'},
                {'T', 'M', 'M'},
        };
        assertEquals(4, pizza.amountOfIngredient(sliceWithFourTomatoes, TOMATO));
    }

    @Test
    void whenNumberOfMushroomsInSliceIsTwoThenAmountOfIngredientReturnsTwo() {
        Character[][] sliceWithFourTomatoes = new Character[][]{
                {'T', 'T', 'T'},
                {'T', 'M', 'M'},
        };
        assertEquals(2, pizza.amountOfIngredient(sliceWithFourTomatoes, MUSHROOM));
    }

}
