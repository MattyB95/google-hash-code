class Intersection(object):

    def __init__(self) -> None:
        self.street_number = 0
        self.street_name = []
        self.street_length = []
        self.times = {}

    def get_times(self):
        for i, l in enumerate(sorted(self.street_length, reverse=True)):
            self.times[l] = 1

    def street_removes(self, street_set: set):
        for j, street in enumerate(self.street_name):
            if street not in street_set:
                self.street_name.remove(street)
                self.street_number -= 1
                self.street_length.remove(self.street_length[j])


def main(file_name: str = 'a'):
    intersections = {}
    with open(f'../res/{file_name}.txt', 'r+') as file:
        _, _, s, v, _ = map(int, file.readline().split())
        streets = [file.readline().split() for _ in range(s)]
        car_paths = [file.readline().split()[1:] for _ in range(v)]
    street_lengths = {}
    travel_lengths = []
    for line in streets:
        street_lengths[line[2]] = line[3]
    for i, element in enumerate(car_paths):
        travel_lengths.append(0)
        for street in element:
            travel_lengths[i] += int(street_lengths[street])
    car_paths = car_paths[:round(len(car_paths) * 0.8)]
    unique_streets = set()
    for p in car_paths:
        for st in p:
            unique_streets.add(st)
    for street in streets:
        _, e, name, length = street
        if int(e) not in intersections.keys():
            intersections[int(e)] = Intersection()
        intersections[int(e)].street_number += 1
        intersections[int(e)].street_name.append(name)
        intersections[int(e)].street_length.append(int(length))
    removal_keys = []
    for key, value in intersections.items():
        value.street_removes(unique_streets)
        if value.street_number == 0:
            removal_keys.append(key)
    for key in removal_keys:
        intersections.pop(key)
    with open(f'../out/{file_name}_output.txt', 'w') as out_file:
        out_file.write(f'{len(intersections):d}\n')
        for key, value in intersections.items():
            out_file.write(f'{key:d}\n')
            out_file.write(f'{value.street_number:d}\n')
            value.get_times()
            for i, streets in enumerate(value.street_name):
                out_file.write(f'{streets:s} ')
                out_file.write(f'{value.times[value.street_length[i]]:d}\n')


if __name__ == '__main__':
    names = ['a', 'b', 'c', 'd', 'e', 'f']
    for c in names:
        main(c)
